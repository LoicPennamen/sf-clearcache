<?php
/**
 * Symfony cache deletion via HTTP
 * Post under /public directory
 * Reach /clearcaches.php in your app
 * Click "Clear caches"
 * Then click "Delete this file"
 */
$cachesDir = __DIR__.'/../var/cache';

function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
					rrmdir($dir. DIRECTORY_SEPARATOR .$object);
				else
					unlink($dir. DIRECTORY_SEPARATOR .$object);
			}
		}
		rmdir($dir);
	}
}

// Delete this
if(key_exists('delete', $_GET)){
	unlink(__FILE__);
	die('File was deleted. Good bye!');
}

// Clear cache
if(is_dir($cachesDir) && key_exists('clear', $_GET)){
	rrmdir($cachesDir);
	echo "The cache was cleared successfully. <br>";
}

// No cache or deleted
elseif(!is_dir($cachesDir)){
	echo "No cache files found. <br>";
}

// Do clear link
else{
	?>
	<a href="/<?=basename(__FILE__)?>?clear=1">
		Clear Symfony caches
	</a> &nbsp;&nbsp;
	<?php
}
?>

<a href="/<?=basename(__FILE__)?>?delete=1">
	Delete this file
</a>
