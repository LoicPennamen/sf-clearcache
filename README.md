# Symfony Cache clearing file #

Simple PHP file to clear Symfony cache when you don't have SSH access to your server.

### How to use ###

* Post file under `/public` directory via FTP
* Reach `/clearcaches.php` from your application root
* Click "Clear caches"
* Then click "Delete this file"

### Who do I talk to? ###

* http://loicpennamen.com
